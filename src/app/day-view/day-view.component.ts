import { Component, OnInit } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { colors } from '../utils/colors';
import { EventService } from '../shared/event.service';

@Component({
  selector: 'app-day-view',
  templateUrl: './day-view.component.html',
  styleUrls: ['./day-view.component.css']
})
export class DayViewComponent implements OnInit {
  viewDate: Date = new Date();
  realEvent: Array<any>
  events: CalendarEvent[]
  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getAll().subscribe(data => {
      this.realEvent = data;
    });
    this.events = [
      {
        title: this.realEvent[0].description,
        start: new Date() ,
      },
      {
        title: 'A non all day event',
        color: colors.blue,
        start: new Date()
      }
    ];
  }

}
