import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/event.service';

@Component({
  selector: 'event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  events: Array<any>;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    this.eventService.getAll().subscribe(data => {
      this.events = data;
    });
  }
}